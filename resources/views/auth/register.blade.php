<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            .content {
                text-align: left;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up From</h3>
            <form action=/daftar method="POST">
                @csrf
                <label>First Name:</label> <br><br>
                <input type="text" id="name" name="first_name">
                <br>
                <label for="last-name">Last Name</label>
                <br>
                <input type="text" id="last-name" name="last_name">
                <br>

                <label>Gender:</label>
                    <p><input type="radio" name="gd">Male</p>
                    <p><input type="radio" name="gd">Female</p>
                    <p><input type="radio" name="gd">Other</p> <br>
            

                <label>Nationality:</label> <br><br>
                    <select name="nas">
                        <option value="WNA">Indonesia</option>
                        <option value="WNI">WNA</option>
                    </select> <br><br>
                    

                    <br><label>Language Spoken:</label> <br><br>
                    <p><input type="checkbox">Bahasa Indonesia</p>
                    <p><input type="checkbox">English</p>
                    <p><input type="checkbox">Other</p><br><br>

                <label>Bio:</label> <br><br>
                    <textarea name="bio" cols="30" rows="10"></textarea> <br>
                    

                    <input type="submit">
            
 
        
    </form>

            </div>
        </div>
    </body>
</html>
